const Task = require('../models/taskSchema')

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
}

//Creating New Task

module.exports.createTask = (reqBody) => {

	let newTask = new Task ({
		name: reqBody.name
	})

	return newTask.save().then((task, err) => {
		if(err){
			console.log(err)
			return false
		} else {
			return task
		}
	})
}

//Delete Task

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndDelete(taskId).then((removedTask, err) => {

		if(err){
			console.log(err)
			return false
		} else {
			return removedTask
		}
	})
}

//update task controller

module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, err) =>
	{
		if(err){
			console.log(err)
			return false
		}

		result.name = newContent.name
		return result.save().then((updatedTask, saveErr) => {

			if(saveErr){
				console.log(saveErr)
				return false
			} else {
				return updatedTask
			}
		})
	})
}

//Activity

//get specific task
module.exports.getOneTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result
	})
}


//update to complete
module.exports.completeTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, err) =>
	{
		if(err){
			console.log(err)
			return false
		}

		result.status = newContent.status
		return result.save().then((completedTask, saveErr) => {

			if(saveErr){
				console.log(saveErr)
				return false
			} else {
				return completedTask
			}
		})
	})
}
