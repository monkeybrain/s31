//Contain all task endpoints for our application

const express = require('express');
const router = express.Router();
const taskController = require('../controllers/taskControllers')

router.get('/', (req, res) => {

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})


//minitask 3
router.post('/createTask', (req, res) => {

	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

//route for deleting
router.delete('/deleteTask/:id', (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

//route for updating
router.put('/update/:id', (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})

//Activity

//route for getting specific task
router.get('/tasks/:id', (req, res) => {
	taskController.getOneTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})


//route for updating to complete
router.put('/tasks/:id/complete', (req, res) => {
	taskController.completeTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})

module.exports = router